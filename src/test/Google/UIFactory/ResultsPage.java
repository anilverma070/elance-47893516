package test.Google.UIFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultsPage {

	private final WebDriver driver;
	
	public ResultsPage(WebDriver d) {
		this.driver= d;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="res")
	WebElement resultDiv;
	
	public void clickFirstResult()
	{
		resultDiv.findElement(By.tagName("a")).click();
	}
}
