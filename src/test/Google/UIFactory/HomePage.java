package test.Google.UIFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	private final WebDriver driver;
	
	public HomePage(WebDriver d) {
		this.driver= d;
		PageFactory.initElements(driver, this);
	}
	
	public ResultsPage searchFor(String searchTerm)
	{
		searchBox.sendKeys(searchTerm);
		searchButton.click();
		return new ResultsPage(driver);
	}
	
	@FindBy(name="q")
	WebElement searchBox;
	
	@FindBy(name="btnG")
	WebElement searchButton;
}