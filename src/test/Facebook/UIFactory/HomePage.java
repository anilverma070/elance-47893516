package test.Facebook.UIFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	private final WebDriver driver;
	
	public HomePage(WebDriver d) {
		this.driver= d;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="email")
	WebElement loginId;
	
	@FindBy(id="pass")
	WebElement loginPassword;
	
	@FindBy(id="loginbutton")
	WebElement signInButton;
	
	@FindBy(name="persistent")
	WebElement rememberMeCheckbox;
	
	public UserHomePage doLogin(String userId, String password, Boolean rememberMe)
	{
		loginId.sendKeys(userId);
		loginPassword.sendKeys(password);
		
		if(rememberMe && !rememberMeCheckbox.isSelected())
			rememberMeCheckbox.click();
		
		signInButton.click();
		return new UserHomePage(driver);
			
	}
	
	public UserHomePage doLogin(String userId, String password)
	{
		return this.doLogin(userId, password, false);
	}
	
}