package test.Facebook.UIFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserHomePage {

	private final WebDriver driver;

	public UserHomePage(WebDriver d) {
		this.driver = d;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "sidebar_navigation_top")
	WebElement topLeftNavigation;

	@FindBy(css = "div#sidebar_navigation_top a")
	WebElement userNameProfileLink;

	@FindBy(xpath = "//div[text()='News Feed']")
	WebElement newsFeedLinkLeftNav;

	@FindBy(xpath = "//div[@class='highlighter']/following-sibling::div//textarea")
	WebElement statusMsgTextArea;

	@FindBy(xpath = "//div[@class='highlighter']/following-sibling::input")
	WebElement statusMsgTextBox;

	@FindBy(xpath = "//button[text()='Post']")
	WebElement statusMsgPostButton;

	@FindBy(xpath = "//div[@id='substream_0']//p")
	WebElement mostRecentStatusMsg;

	public String getAccountName() {
		return userNameProfileLink.getText();
	}

	public void postStatudUpdate(String statusMessage) {
		newsFeedLinkLeftNav.click();
		statusMsgTextArea.click();
		statusMsgTextArea.sendKeys(statusMessage);
		statusMsgPostButton.click();
		try {
			Thread.sleep(1000);
			statusMsgTextArea.clear();
			driver.navigate().refresh();
		} catch (Exception e) {
		}
	}

	public String getMostRecentStatusMsg() {
		return mostRecentStatusMsg.getText();
	}
}