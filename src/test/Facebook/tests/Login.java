package test.Facebook.tests;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

import test.Facebook.UIFactory.HomePage;
import test.Facebook.UIFactory.UserHomePage;

public class Login {

	WebDriver driver = null;

	@Test
	public void testPostStatusMsg() {
		String userId= "kjadhgjkeahgjkahkg@reallymymail.com";
		String userPassword= "anil@automation";
		String status="Hello World!";
		
		//Facebook does not allows same msg to be posed again and again, so appending a random number
		//This can be removed if msg passed in unique
		status= status + new Random().nextInt(100);
		
		HomePage home = new HomePage(driver);
		UserHomePage userHome = home.doLogin(userId, userPassword);
		userHome.postStatudUpdate(status);
		Assert.assertEquals(userHome.getMostRecentStatusMsg(), status);
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}
}