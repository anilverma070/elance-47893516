package test.wallethub.UIFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CompanyProfilePage extends Page
{
    public CompanyProfilePage(WebDriver d)
    {
        this.driver= d;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath= "//span[contains(@class,'wh-rating rating_')]")
    WebElement currentRatings;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']")
    WebElement choiceRating;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']//a[text()='1']")
    WebElement choiceRating1Star;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']//a[text()='2']")
    WebElement choiceRating2Star;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']//a[text()='3']")
    WebElement choiceRating3Star;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']//a[text()='4']")
    WebElement choiceRating4Star;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']//a[text()='5']")
    WebElement choiceRating5Star;

    @FindBy(xpath= "//div[@class='wh-rating-choices-holder']/em")
    WebElement choiceRatingMsg;

    @FindBy(xpath= "//div[@class='wh-overlay-frame login wh-overlay-frame-show']")
    WebElement loginOverlayPopUp;

    public Page chooseRating(int stars) throws InterruptedException
    {
        Actions action= new Actions(driver);
        action.moveToElement(currentRatings);
        switch (stars)
        {
            case 1:
                action.click(choiceRating1Star);
                break;
            case 2:
                action.click(choiceRating2Star);
                break;
            case 3:
                action.click(choiceRating3Star);
                break;
            case 4:
                action.click(choiceRating4Star);
                break;
            case 5:
                action.click(choiceRating5Star);
                break;

            default:
                break;
        }
        action.build().perform();
        if (loginOverlayPopUp.isDisplayed())
            return new SignInPopUp(driver);
        else
            return this;
    }
}
