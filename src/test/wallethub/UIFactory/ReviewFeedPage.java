package test.wallethub.UIFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReviewFeedPage extends Page
{
    public ReviewFeedPage(WebDriver d)
    {
        this.driver= d;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id= "review-product")
    public WebElement lstPolicyType;

    public static ReviewFeedPage gotoReviewFeedPage(WebDriver d, String username)
    {
        String url= "http://wallethub.com/profile/" + username + "/activity/";
        d.get(url);
        return new ReviewFeedPage(d);
    }
}
