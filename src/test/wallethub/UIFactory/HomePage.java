package test.wallethub.UIFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends Page
{

    public HomePage(WebDriver d)
    {
        this.driver= d;
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText= "Sign In")
    public WebElement loginLnk;

    @FindBy(linkText= "Join Wallet Hub")
    public WebElement joinLnk;

    @FindBy(linkText= "My Account")
    public WebElement accountsLnk;

    @FindBy(linkText = "Activity")
    public WebElement activityLnkInAccountsMenu;

    public ActivityPage gotoActivityPage()
    {
        Actions action= new Actions(driver);
        action.moveToElement(accountsLnk);
        action.click(activityLnkInAccountsMenu);
        action.build().perform();
        return new ActivityPage(driver);
    }
}