package test.wallethub.UIFactory;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ReviewsPage extends Page
{
    public ReviewsPage(WebDriver d)
    {
        this.driver= d;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id= "review-product")
    public WebElement lstPolicyType;

    @FindBy(partialLinkText= "Write a full review and get")
    public WebElement writeFullReviewLnk;

    @FindBy(id= "review-content")
    public WebElement reviewContentTextBox;

    @FindBy(css= "form#reviewform input[value='Submit']")
    public WebElement reviewContentSubmitBtn;

    @FindBy(id= "publishfb")
    public WebElement publishToFacebookCheckBox;

    @FindBy(className= "pagetitle")
    public WebElement reviewPublishConfirmationMsg;

    @FindBy(css= ".pagetitle a")
    public WebElement companyBeingReviewedLnk;

    public void selectPolicyType(String policy)
    {
        Select policyList= new Select(lstPolicyType);
        policyList.selectByValue(policy);
    }

    public void writeReview(String reviewText)
    {
        reviewContentTextBox.click();
        reviewContentTextBox.clear();
        reviewContentTextBox.sendKeys(reviewText);
    }

    public String writeRandomReview(int minNoOfChars)
    {
        String reviewText= "Anilverma070 testing it";
        do
        {
            reviewText= reviewText + " " + RandomStringUtils.randomAlphanumeric(10);
        } while (reviewText.length() < minNoOfChars);

        this.writeReview(reviewText);
        return reviewText;
    }

    public String writeRandomReviewAndSubmit(int minNoOfChars)
    {
        try
        {if(writeFullReviewLnk.isDisplayed())
            writeFullReviewLnk.click();
        }
        catch (Exception e)
        {}

        String reviewText= this.writeRandomReview(minNoOfChars);
        this.submitReview();
        return reviewText;
    }

    public void submitReview()
    {
        this.submitReview(false);
    }

    public void submitReview(boolean publishToFacebook)
    {
        if (publishToFacebook && !publishToFacebookCheckBox.isSelected())
            publishToFacebookCheckBox.click();
        else if (!publishToFacebook && publishToFacebookCheckBox.isSelected())
            publishToFacebookCheckBox.click();

        reviewContentSubmitBtn.click();
    }

    public String getCompanyBeingReviewed()
    {
        return companyBeingReviewedLnk.getText();
    }

    public String getReviewPublishConfirmationMsg()
    {
        return reviewPublishConfirmationMsg.getText();
    }
}
