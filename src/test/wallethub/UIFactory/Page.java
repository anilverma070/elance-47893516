package test.wallethub.UIFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Page
{
    protected WebDriver driver;
    
    public String getText()
    {
        return driver.findElement(By.xpath("//*")).getText();
    }
    
    public void goTo(String url)
    {
        driver.get(url);
    }
}