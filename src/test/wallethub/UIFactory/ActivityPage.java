package test.wallethub.UIFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.google.common.base.Ascii;

public class ActivityPage extends Page
{

    public ActivityPage(WebDriver d)
    {
        this.driver= d;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath= "//div[contains(@class,'feed-content')]/p[@class='feeddesc']")
    public WebElement mostRecentReviewDesc;
    
    @FindBy(xpath= "//div[contains(@class,'feed-content')]/span[contains(@class,'wh-rating')]")
    public WebElement mostRecentReviewRating;

    public String getMostRecentReviewRating()
    {
        return mostRecentReviewRating.getText();
    }
    
    public String getMostRecentReviewText()
    {
        String text= mostRecentReviewDesc.getText();
        text= text.replace(Character.toChars(8230)[0], ' ').trim();
        return text;
    }
}
