package test.wallethub.UIFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPopUp extends Page
{
    public SignInPopUp(WebDriver d)
    {
        this.driver= d;

        // TODO PROBLEM here
        // Although there should be just one iframe, but webdriver reports more than 7
        // So we iterate and choose correct one based on which one has has username field
        d.switchTo().defaultContent();
        List<WebElement> loginFrames= d.findElements(By.xpath("//iframe|//frame"));
        for (WebElement loginFrame: loginFrames)
        {
            d= d.switchTo().frame(loginFrame);
            if (d.getPageSource().contains("overlay-username"))
            {
                this.driver= d;
                break;
            }
            d= d.switchTo().defaultContent();
        }
        PageFactory.initElements(driver, this);
    }

    @FindBy(id= "overlay-username")
    WebElement loginId;

    @FindBy(id= "overlay-password")
    WebElement loginPassword;

    @FindBy(name= "wp-submit")
    WebElement signInButton;

    @FindBy(id= "rememberme")
    WebElement rememberMeCheckbox;

    public void doLogin(String userId, String password, Boolean rememberMe)
    {
        loginId.sendKeys(userId);
        loginPassword.sendKeys(password);

        if (rememberMe && !rememberMeCheckbox.isSelected())
            rememberMeCheckbox.click();

        signInButton.click();
    }

    public void doLogin(String userId, String password)
    {
        this.doLogin(userId, password, false);
    }
}
