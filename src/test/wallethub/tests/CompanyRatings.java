package test.wallethub.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

import test.wallethub.UIFactory.ActivityPage;
import test.wallethub.UIFactory.CompanyProfilePage;
import test.wallethub.UIFactory.HomePage;
import test.wallethub.UIFactory.ReviewsPage;
import test.wallethub.UIFactory.SignInPopUp;

public class CompanyRatings
{
    WebDriver driver= null;

    @Test
    public void testPostAReview() throws InterruptedException
    {
        // All variables across the test
        String userId= "anilverma070";
        String userPassword= "testaccount";
        String companyUrl= "http://wallethub.com/profile/test_insurance_company/";
        int companyRating= 5;
        String policyType= "Health";

        // Go to home page and navigate to specific company page
        HomePage home= new HomePage(driver);
        home.goTo(companyUrl);

        // Select a rating and sign in when asked
        // TODO
        // BUG 1: If you have already provided a rating for selected company
        // Whatever, rating you choose here, it gets reset after you sign in to previously applied rating
        CompanyProfilePage companyProfile= new CompanyProfilePage(driver);
        SignInPopUp signIn= (SignInPopUp) companyProfile.chooseRating(companyRating);
        signIn.doLogin(userId, userPassword);

        ReviewsPage review= new ReviewsPage(driver);
        String companyName= review.getCompanyBeingReviewed();

        // Select a policy type and submit a review
        review.selectPolicyType(policyType);
        String reviewText= review.writeRandomReviewAndSubmit(200);

        // Verify confirmation message is correct
        String successMsg= review.getReviewPublishConfirmationMsg();
        Assert.assertEquals(successMsg, "Thank you! Your review for " + companyName + " has been posted.");

        // Go to activity page and assert that posted feed appears correctly on activity page
        // TODO
        // BUG 2: Even though application asks for min 200 characters, only 170 are displayed on activity page
        // Minimum no of asked characters must be displayed on feed page, and should be trimmed
        // only when they exceed a limit significantly higher than minimum limit
        ActivityPage activity= home.gotoActivityPage();
        if (reviewText.length() > 150)
            Assert.assertTrue(reviewText.startsWith(activity.getMostRecentReviewText()));
        else
            Assert.assertEquals(activity.getMostRecentReviewText(), reviewText);
        // Assert.assertEquals(activity.getMostRecentReviewRating(), "Rate: " + companyRating);
    }

    @BeforeClass
    public void beforeClass()
    {}

    @AfterClass
    public void afterClass()
    {}

    @BeforeTest
    public void beforeTest()
    {
        driver= new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://wallethub.com/");
    }

    @AfterTest
    public void afterTest()
    {
        driver.quit();
    }

    @BeforeSuite
    public void beforeSuite()
    {}

    @AfterSuite
    public void afterSuite()
    {}
}
